

//filter

function filterBy(arr, type){
  let newArr = arr.filter(function(item){
    return typeof(item) !== type;
  }); 
  console.log(newArr);
  return newArr 
  };
filterBy(['hello', 'world', 23, '23', null], 'string');

//foreach

function filterBy(arr, type) {
  let newArr = []
  arr.forEach(item => {
    if(typeof (item)!== type){
      newArr.push(item);
    };
  }); 
  console.log(newArr);
  return newArr;
};   
filterBy(['hello', 'world', 23, '23', null], 'string');



